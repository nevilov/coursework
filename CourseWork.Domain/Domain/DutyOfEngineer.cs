using System;

namespace CourseWork.Domain.Domain
{
    public class DutyOfEngineer
    {
        public Guid EngineerId { get; set; }
        
        public string ClassCipher { get; set; }
        
        public DateTime DutyDate { get; set; }
        
        public int NumberOfFaults { get; set; }
    }
}