using System;
using System.Threading.Tasks;
using CourseWork.Application.Services.Interfaces;
using CourseWork.Domain.Domain;
using CourseWork.Models;
using Microsoft.AspNetCore.Mvc;

namespace CourseWork.Controllers
{
    [Route("[controller]/[action]")]
    public class DutiesOfEngineerController : Controller
    {
        private readonly IDutyService _dutyService;

        public DutiesOfEngineerController(IDutyService dutyService)
        {
            _dutyService = dutyService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDutyRecord(DutyOfEngineerViewModel model)
        {
            model.EngineerId = Guid.NewGuid();
            await _dutyService.Create(new DutyOfEngineer()
            {
                EngineerId = model.EngineerId,
                ClassCipher = model.ClassCipher,
                NumberOfFaults = model.NumberOfFaults,
                DutyDate = model.DutyDate
            });
            return Redirect("/Home/Index");
        }
    }
}