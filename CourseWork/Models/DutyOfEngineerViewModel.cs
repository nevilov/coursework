using System;

namespace CourseWork.Models
{
    public class DutyOfEngineerViewModel
    {
        public Guid EngineerId { get; set; }
        
        public string ClassCipher { get; set; }
        
        public DateTime DutyDate { get; set; }
        
        public int NumberOfFaults { get; set; }
    }
}