using System.Collections.Generic;
using System.Threading.Tasks;
using CourseWork.Application.Services.Interfaces;
using CourseWork.Domain.Domain;
using CourseWork.Infrastructure.DataAccess.Repositories.Interfaces;

namespace CourseWork.Application.Services.Implementations
{
    public class DutyService : IDutyService
    {
        private readonly IDutyRepository _repository;

        public DutyService(IDutyRepository repository)
        {
            _repository = repository;
        }
        public async Task<IEnumerable<DutyOfEngineer>> Get()
        {
            return await _repository.Get();
        }

        public async Task Create(DutyOfEngineer dutyOfEngineer)
        {
            await _repository.Create(dutyOfEngineer);
        }

        public async Task Update(DutyOfEngineer dutyOfEngineer)
        {
            var duty = await _repository.GetByKey(dutyOfEngineer);
            duty.NumberOfFaults = dutyOfEngineer.NumberOfFaults;
            await _repository.Save();
        }

        public async Task Delete(DutyOfEngineer dutyOfEngineer)
        {
            await _repository.Delete(dutyOfEngineer);
        }
    }
}