using System.Collections.Generic;
using System.Threading.Tasks;
using CourseWork.Domain.Domain;

namespace CourseWork.Application.Services.Interfaces
{
    public interface IDutyService
    {
        Task<IEnumerable<DutyOfEngineer>> Get();
        Task Create(DutyOfEngineer dutyOfEngineer);
        Task Update(DutyOfEngineer dutyOfEngineer);
        Task Delete(DutyOfEngineer dutyOfEngineer);
    }
}