using CourseWork.Application.Services.Implementations;
using CourseWork.Application.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CourseWork.Application.Extensions
{
    public static class ApplicationConfiguration
    {
        public static IServiceCollection AddApplicationConfiguration(this IServiceCollection service)
        {
            service.AddScoped<IDutyService, DutyService>();
            return service;
        }
    }
}