using CourseWork.Infrastructure.DataAccess;
using CourseWork.Infrastructure.DataAccess.Repositories.Implementations;
using CourseWork.Infrastructure.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CourseWork.Infrastructure.Extensions
{
    public static class EntityFrameworkConfiguration
    {
        public static IServiceCollection AddEntityFramework(this IServiceCollection serivce
            , IConfiguration configuration)
        {
            serivce.AddDbContext<ApplicationDbContext>(a =>
                a.UseNpgsql(configuration.GetConnectionString("Default")));

            serivce.AddScoped<IDutyRepository, DbDutyOfEngineerRepository>();
            
            return serivce;
        }
    }
}