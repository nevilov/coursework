using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CourseWork.Domain.Domain;
using CourseWork.Infrastructure.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CourseWork.Infrastructure.DataAccess.Repositories.Implementations
{
    public class DbDutyOfEngineerRepository : IDutyRepository
    {
        private readonly ApplicationDbContext _dbContext;
        
        public DbDutyOfEngineerRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<IEnumerable<DutyOfEngineer>> Get()
        {
            return await _dbContext.DutyOfEngineers.ToListAsync();
        }

        public async Task Create(DutyOfEngineer dutyOfEngineer)
        {
            await _dbContext.DutyOfEngineers.AddAsync(dutyOfEngineer);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Save()
        {
            await _dbContext.SaveChangesAsync();
        }

        public async Task<DutyOfEngineer> GetByKey(DutyOfEngineer dutyOfEngineer)
        {
             return await _dbContext.DutyOfEngineers
                .Where(a => a.EngineerId == dutyOfEngineer.EngineerId)
                .Where(a => a.DutyDate == dutyOfEngineer.DutyDate)
                .Where(a => a.ClassCipher == dutyOfEngineer.ClassCipher).FirstOrDefaultAsync();
        }

        public async Task Delete(DutyOfEngineer dutyOfEngineer)
        {
            var dutyRecord = await _dbContext.DutyOfEngineers
                .Where(a => a.EngineerId == dutyOfEngineer.EngineerId)
                .Where(a => a.DutyDate == dutyOfEngineer.DutyDate)
                .Where(a => a.ClassCipher == dutyOfEngineer.ClassCipher).FirstOrDefaultAsync();

            if (dutyRecord is not null)
            {
                _dbContext.DutyOfEngineers.Remove(dutyOfEngineer);
            }
        }
    }
}