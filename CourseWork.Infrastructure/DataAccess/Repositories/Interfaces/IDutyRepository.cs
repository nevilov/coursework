using System.Collections.Generic;
using System.Threading.Tasks;
using CourseWork.Domain.Domain;

namespace CourseWork.Infrastructure.DataAccess.Repositories.Interfaces
{
    public interface IDutyRepository
    {
        Task<IEnumerable<DutyOfEngineer>> Get();
        Task Create(DutyOfEngineer dutyOfEngineer);
        Task Save();
        Task<DutyOfEngineer> GetByKey(DutyOfEngineer dutyOfEngineer);
        Task Delete(DutyOfEngineer dutyOfEngineer);
    }
}