using CourseWork.Domain.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace CourseWork.Infrastructure.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<DutyOfEngineer>()
                .HasKey(a => new { a.EngineerId, a.ClassCipher, a.DutyDate });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<DutyOfEngineer> DutyOfEngineers { get; set; }
    }
}