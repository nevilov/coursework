﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CourseWork.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DutyOfEngineers",
                columns: table => new
                {
                    EngineerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClassCipher = table.Column<string>(type: "text", nullable: false),
                    DutyDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    NumberOfFaults = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DutyOfEngineers", x => new { x.EngineerId, x.ClassCipher, x.DutyDate });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DutyOfEngineers");
        }
    }
}
